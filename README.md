## CTU FEE Computer Architectures Course Support Materials for Students

The course pages: [https://cw.fel.cvut.cz/wiki/courses/b35apo/start](https://cw.fel.cvut.cz/wiki/courses/b35apo/start)

The directory `seminaries` provides templates and test applications
for seminaries. The content can be modified to experiment with examples
and even modifications can be archived into your personal git.
But the work which is intended to be shared with your tutor should
be placed into directory `work`.
